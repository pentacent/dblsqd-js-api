# dblsqd-api: DBLSQD API Client
This module provides a Promise-based API for accessing the functionality of DBLSQD.

## Installing
Install *dblsqd-api* by adding it to your `package.json` file or via the
command-line: `npm i --save-dev dblsqd-api`.

## Usage

Here is a basic example of how to create a new App, with a Channel with one Release
and upload an Artifact:

```javascript
const dblsqdApi = require("dblsqd-api")

async function main() {
    //Retrieve a new API key. You could store this key and re-use it later.
    let apiKey = await api.getApiKey("user@example.org", "secure-password-123")

    //dblsqdApi.init makes the apiKey available for later calls
    dblsqdApi.init(apiKey)

    //All methods for interacting with the DBLSQD server are implemented as
    //Promises. You can also use .then() chaining instead of async/await.
    const store = await dblsqdApi.createStoreS3("my-store", {
        access_key_id: "***",
        secret_key: "***",
        bucket: "octaplay-bucket"
    })
    const app      = await dblsqdApi.createApp("octaplay")
    const channel  = await dblsqdApi.createChannel(app.uuid, "stable")
    const release  = await dblsqdApi.createRelease(channel.uuid, "1.0.0", "Here goes the changelog")
    const artifact = await dblsqdApi.createArtifact(release.uuid)

    console.log("Artifact uploaded", artifact)
}

main()
```