/**
 * @module dblsqdApi
 */

"use strict"
const jsonrpc = require("./dblsqd-api/jsonrpc.js")
const defaultEndpoint = "https://api.dblsqd.com/v1/jsonrpc"
var config = {}


/**
 * Initializes the API.
 * This method must be called before making any calls other than
 * {@link authenticate} or {@link register}.
 * @alias module:dblsqdApi.init
 * @param {string} apiKey The API key retrieved with {@link authenticate}.
 * @param {string} [endpoint="https://api.dblsqd.com/v1/jsonrpc"] The DBLSQD v1
 * JSONRPC endpoint.
 */
const init = function(apiKey, endpoint) {
    config.endpoint = endpoint || defaultEndpoint
    config.apiKey = apiKey
}


/**
 * Registers a new DBLSQD account.
 * @alias module:dblsqdApi.register
 * @param {string} email
 * @param {string} password
 * @param {Object} info
 * @param {string} info.first_name
 * @param {string} [info.last_name]
 * @param {string} [info.company]
 * @param {string} [endpoint="https://api.dblsqd.com/v1/jsonrpc"] The DBLSQD v1
 * JSONRPC endpoint.
 * @returns {Promise}
 */
const register = function(email, password, info, endpoint) {
    let params = {email: email, password: password, info: info}
    endpoint = endpoint || defaultEndpoint
    return jsonrpc.post(endpoint, "register", params)
}


/**
 * Retrieves a new API Key from the DBLSQD server.
 * @alias module:dblsqdApi.getApiKey
 * @param {string} email DBLSQD account email.
 * @param {string} password DBLSQD account password.
 * @param {string} [endpoint="https://api.dblsqd.com/v1/jsonrpc"] The DBLSQD v1
 * JSONRPC endpoint.
 * @returns {Promise}
 */
const getApiKey = function(email, password, endpoint) {
    let params = {email: email, password: password}
    endpoint = endpoint || defaultEndpoint
    return jsonrpc.post(endpoint, "getApiKey", params)
}


/**
 * Revokes the API Key previously set with {@link init}.
 * @alias module:dblsqdApi.revokeApiKey
 * @returns {Promise}
 */
const revokeApiKey = function() {
  let opts = {api_key: config.apiKey}
  return jsonrpc.post(config.endpoint, "revokeApiKey", opts, config.apiKey)
}

/**
 * Returns information about the user associated with the API Key
 * @returns {Promise}
 */
const getApiKeyUser = function() {
  return jsonrpc.post(config.endpoint, "getApiKeyUser", {}, config.apiKey)
}


const listRecords = function(type) {
    let opts = [type]
    return jsonrpc.post(config.endpoint, "getRecords", opts, config.apiKey)
}


/**
 * Retrieves information about all Apps.
 * @alias module:dblsqdApi.listApps
 * @returns {Promise}
 */
const listApps = () => listRecords("app")


/**
 * Retrieves information about all Channels.
 * Filters by App if appUUID is provided.
 * @alias module:dblsqdApi.listChannels
 * @param {string} [appUUID] App UUID
 * @returns {Promise}
 */
const listChannels = appUUID => listRecords("channel")
    .then(channels => typeof appUUID === "undefined" ?
        channels :
        channels
            .filter(channel => channel.app_uuid === appUUID)
    )


/**
 * Retrieves information about all Releases.
 * Filters by Channel if channelUUID is provided.
 * @alias module:dblsqdApi.listReleases
 * @param {string} [channelUUID] Channel UUID
 * @returns {Promise}
 */
const listReleases = channelUUID => listRecords("release")
    .then(releases => typeof channelUUID === "undefined" ?
        releases :
        releases
            .filter(release => release.channel_uuid === channelUUID)
    )


/**
 * Retrieves information about all Artifacts.
 * Filters by Release if releaseUUID is provided.
 * @alias module:dblsqdApi.listArtifacts
 * @param {string} [releaseUUID] Release UUID
 * @returns {Promise}
 */
const listArtifacts = releaseUUID => listRecords("file")
    .then(artifacts => typeof releaseUUID === "undefined" ?
        artifacts :
        artifacts
            .filter(artifacts => artifacts.release_uuid === releaseUUID)
    )



/**
 * Retrieves information about all Stores.
 * @alias module:dblsqdApi.listStores
 * @returns {Promise}
 */
const listStores = () => listRecords("store")


/**
 * Retrieves information about an App.
 * @param {string} name App name.
 * @returns {Promise}
 */
const findApp = name => new Promise((resolve, reject) => listApps()
    .then(apps => {
        let app = apps.find(app => app.name === name)
        app ? resolve(app) : reject(`App not found: "${name}"`)
    })
    .catch(reason => reject(reason))
)

/**
 * Retrieves information about a Store.
 * @param {string} name Store name.
 * @returns {Promise}
 */
const findStore = name => new Promise((resolve, reject) => listStores()
    .then(stores => {
        let store = stores.find(store => store.name === name)
        store ? resolve(store) : reject(`Store not found: "${name}"`)
    })
    .catch(reason => reject(reason))
)


/**
 * Retrieves information about a Channel.
 * @param {string} appName App name.
 * @param {string} channelName Channel name.
 * @returns {Promise}
 */
const findChannel = (appName, channelName) => new Promise((resolve, reject) => {
    findApp(appName).then(app => listChannels().then(channels => {
        let channel = channels.find(channel =>
            channel.name === channelName &&
            channel.app_uuid === app.uuid
        )
        channel ?
            resolve(channel) :
            reject(`Channel not found: "${channelName}" in "${appName}"`)
    }))
    .catch(reason => reject(reason))
})

/**
 * Retrieves information about a Release.
 * @param {string} appName App name.
 * @param {string} channelName Channel name.
 * @param {string} version Release version.
 * @returns {Promise}
 */
const findRelease = (appName, channelName, version) => new Promise((resolve, reject) => {
    findChannel(appName, channelName).then(channel => listReleases().then(releases => {
        let release = releases.find(release =>
            release.version === version &&
            release.channel_uuid === channel.uuid
        )
        release ?
            resolve(release) :
            reject(`Release not found: "${version}" in "${channelName}" in "${appName}"`)
    }))
    .catch(reason => reject(reason))
})

/**
 * Retrieves information about an Artifact.
 * @param {string} appName App name.
 * @param {string} channelName Channel name.
 * @param {string} version Release version.
 * @param {string} os Operating system.
 * @param {string} arch Processor architecture.
 * @param {string} [type="standalone"] Artifact Type.
 * @param {string} [deltaFor] Patch target version.
 * @returns {Promise}
 */
const findArtifact = (appName, channelName, version, os, arch, type, deltaFor) =>  new Promise((resolve, reject) => {
    type = type || "standalone"
    findRelease(appName, channelName, version)
        .then(release => listArtifacts(release.uuid))
        .then(artifacts => {
            const artifact = artifacts.find(artifact =>
                artifact.os === os &&
                artifact.arch === arch &&
                artifact.type === type &&
                artifact.patch_version === deltaFor
            )
            artifact ?
            resolve(artifact) :
            reject(`Artifact not found: "${os}:${arch}" ("${type}") for "${version}" in "${channelName}" in "${appName}"`)
        })
        .catch(reason => reject(reason))
})

///////////////////////////////////////////
const createRecord = function(type, data) {
    let opts = [type, data]
    return jsonrpc.post(config.endpoint, "createRecord", opts, config.apiKey)
}

/**
 * Creates a new App.
 * @alias module:dblsqdApi.createApp
 * @param {string} name App name.
 * @returns {Promise}
 */
const createApp = function(name) {
    return createRecord("app", {name: name})
}

/**
 * Creates a new Channel.
 * @alias module:dblsqdApi.createChannel
 * @param {string} appUUID App UUID.
 * @param {string} name Channel name.
 * @returns {Promise}
 */
const createChannel = function(appUUID, name) {
    return createRecord("channel", {app_uuid: appUUID, name: name})
}

/**
 * Creates a new Release.
 * @alias module:dblsqdApi.createRelease
 * @param {string} channelUUID Channel UUID.
 * @param {string} version Release version.
 * @param {string} changelog Release changelog.
 * @returns {Promise}
 */
const createRelease = function(channelUUID, version, changelog) {
    return createRecord("release", {
        channel_uuid: channelUUID,
        version: version,
        changelog: changelog
    })
}

/**
 * Creates a new Artifact.
 * @alias module:dblsqdApi.createArtifact
 * @param {string} releaseUUID Release UUID.
 * @param {string} storeUUID Store UUID.
 * @param {path} path Release version.
 * @param {object} data Artifact metadata.
 * @param {string} data.os Operating system.
 * @param {string} data.arch Processor architecture.
 * @param {string} [data.type="standalone"] Artifact Type.
 * @param {string|boolean} [data.patch=false] Set to true to mark this
 * Artifact as a patch. When using Semantic Versioning, an operator for
 * targeting previous Release versions can also be used.
 * @returns {Promise}
 */
const createArtifact = function(releaseUUID, storeUUID, path, data, progressCallback) {
    let params = {release_uuid: releaseUUID, store_uuid: storeUUID}
    Object.getOwnPropertyNames(data).forEach(key => params[key] = data[key])
    params = {data: params}
    return jsonrpc.upload(config.endpoint, "uploadFile", params, [path], config.apiKey, progressCallback)
}

/**
 * Attaches an external Artifact.
 * @alias module:dblsqdApi.attachExternalArtifact
 * @param {string} releaseUUID Release UUID.
 * @param {string} storeUUID Store UUID.
 * @param {path} url Release version.
 * @param {object} data Artifact metadata. Possible options are identical to createArtifact.
 */
const attachExternalArtifact = function(releaseUUID, url, data) {
    let params = {release_uuid: releaseUUID, url: url}
    Object.getOwnPropertyNames(data).forEach(key => params[key] = data[key])
    return jsonrpc.post(config.endpoint, "attachExternalFile", [params], config.apiKey)
}

/**
 * Creates a new S3 Store.
 * @alias module:dblsqdApi.createStoreS3
 * @param {string} name Store name.
 * @param {object} config Store configuration metadata.
 * @param {string} config.host S3 host name.
 * @param {number} config.port S3 port.
 * @param {string} config.region S3 region.
 * @param {string} config.access_key_id Acces Key Id.
 * @param {string} config.secret_access_key Secret Access Key.
 * @param {string} config.bucket Bucket name.
 * @param {boolean} [config.https=true] Use HTTPS.
 * @returns {Promise}
 */
const createStoreS3 = function(name, config) {
    return createRecord("store", {
        name: name,
        provider: "s3",
        config: config
    })
}

/**
 * Creates a new SFTP Store.
 * @alias module:dblsqdApi.createStoreSFTP
 * @param {string} name Store name.
 * @param {object} config Store configuration metadata.
 * @param {string} config.host Host name.
 * @param {number} [config.port=22] Port.
 * @param {string} config.user User name.
 * @param {string} [config.password] User password.
 * @param {string} [config.user_key] Private user key. Can be used instead of password.
 * @param {string} [config.host_key] Public host key.
 * @param {string} [config.directory] Remote directory.
 *                                    Defaults to SFTP working directory.
 * @param {string} config.base_url URL base for Artifact URLs on this Store.
 * @returns {Promise}
 */
const createStoreSFTP = function(name, config) {
    return createRecord("store", {
        name: name,
        provider: "sftp",
        config: config
    })
}


const deleteRecord = function(type, uuid, options) {
    let opts = [type, uuid, options]
    return jsonrpc.post(config.endpoint, "deleteRecord", opts, config.apiKey)
}

/**
 * Deletes an App and all its Channels and Releases.
 * Remote Artifact files are deleted from Stores if purge is set to true.
 * @alias module:dblsqdApi.deleteApp
 * @param {string} uuid App UUID.
 * @param {boolean} [purge] Delete remote files.
 */
const deleteApp = (uuid, purge) =>
    deleteRecord("app", uuid, {purge: purge === true})

/**
 * Deletes an Channel and all its Releases.
 * Remote Artifact files are deleted from Stores if purge is set to true.
 * @alias module:dblsqdApi.deleteChannel
 * @param {string} uuid Channel UUID.
 * @param {boolean} [purge] Delete remote files.
 */
const deleteChannel = (uuid, purge) =>
    deleteRecord("channel", uuid, {purge: purge === true})

/**
 * Deletes a Release.
 * Remote Artifact files are deleted from Stores if purge is set to true.
 * @alias module:dblsqdApi.deleteRelease
 * @param {string} uuid Release UUID
 * @param {boolean} [purge] Delete remote files.
 */
const deleteRelease = (uuid, purge) =>
    deleteRecord("release", uuid, {purge: purge === true})

/**
 * Deletes an Artifact. The file will remain stored on the remote server unless
 * purge is set to true.
 * @alias module:dblsqdApi.deleteArtifact
 * @param {string} uuid Artifact UUID.
 * @param {boolean} [purge] Delete remote file.
 */
const deleteArtifact = (uuid, purge) =>
    deleteRecord("file", uuid, {purge: purge === true})


/**
 * Deletes a Store.
 * If purge is set to true, Artifacts are deleted and purged from the remote
 * server. Otherwise, Artifacts associated with the Store remain available
 * to Feeds in DBLSQD and only their reference to the Store will be deleted.
 * @alias module:dblsqdApi.deleteStore
 * @param {string} uuid Store UUID.
 * @param {boolean} [purge] Delete Artifacts and remote files.
 */
const deleteStore = (uuid, purge) =>
    deleteRecord("store", uuid, {purge: purge === true})


/**
 * Retrieves the Feeds base url for an App.
 * @alias module:dblsqdApi.getAppBaseUrl
 * @param {string} uuid App UUID
 * @returns {Promise}
 */
const getAppBaseUrl = function(uuid) {
    let opts = [uuid]
    return jsonrpc.post(config.endpoint, "getAppBaseUrl", opts, config.apiKey)
}


module.exports = {
    init: init,
    register: register,
    getApiKey: getApiKey,
    revokeApiKey: revokeApiKey,
    getApiKeyUser: getApiKeyUser,

    createApp: createApp,
    createChannel: createChannel,
    createRelease: createRelease,
    createStoreS3: createStoreS3,
    createStoreSFTP: createStoreSFTP,
    createArtifact: createArtifact,
    attachExternalArtifact: attachExternalArtifact,

    listApps: listApps,
    listChannels: listChannels,
    listReleases: listReleases,
    listArtifacts: listArtifacts,
    listStores: listStores,

    findApp: findApp,
    findStore: findStore,
    findChannel: findChannel,
    findRelease: findRelease,
    findArtifact: findArtifact,

    deleteApp: deleteApp,
    deleteChannel: deleteChannel,
    deleteRelease: deleteRelease,
    deleteArtifact: deleteArtifact,
    deleteStore: deleteStore,

    getAppBaseUrl: getAppBaseUrl
}
