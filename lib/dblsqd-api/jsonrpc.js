"use strict";

/**
 * @module
 * @private
 */

const crypto = require("crypto")
const fs = require("fs")
const request = require("./request.js")

const createJSONRPC = (method, params) => {
    let id = crypto.randomBytes(16).toString("base64")
    return {
        "jsonrpc": "2.0",
        "method": method,
        "params": params,
        "id": id
    }
}

const makePromise = requestPromise => new Promise((resolve, reject) => {
    requestPromise.then(value => {
        try {
            let json = JSON.parse(value)
            if (json.jsonrpc !== "2.0") {
                reject(`Not a valid JSON-RPC response: ${value}`)
            } else if (json.result) {
                resolve(json.result)
            } else if (json.error) {
                reject(json.error)
            } else {
                reject(`Not a valid JSON-RPC response: ${value}`)
            }
        } catch(error) {
            reject(`Could not parse response: ${error}`)
        }
    })
    requestPromise.catch(reason => reject(reason))
})

const post = function(endpoint, method, params, apiKey) {
    let json = createJSONRPC(method, params)
    let extraHeaders = apiKey ? {"Authorization": `Bearer: ${apiKey}`} : {}
    let requestPromise = request.post(endpoint, json, extraHeaders)
    return makePromise(requestPromise)
}

const upload = function(endpoint, method, params, uploads, apiKey, progressCallback) {
    let json = createJSONRPC(method, params)
    uploads = Array.isArray(uploads) ? uploads : [uploads]
    let extraHeaders = apiKey ? {"Authorization": `Bearer: ${apiKey}`} : {}
    let requestPromise = request.upload(endpoint, uploads, json, extraHeaders, progressCallback)
    return makePromise(requestPromise)
}

module.exports = {
    post: post,
    upload: upload
}
