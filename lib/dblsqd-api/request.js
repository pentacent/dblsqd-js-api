"use strict";

/**
 * @module
 * @private
 */

const fs = require("fs")
const http = require("http")
const https = require("https")
const path = require("path")
const url = require("url")
const FormData = require("form-data")

const getOpts = (method, endpoint) => {
    let u = url.parse(endpoint)
    return {
        protocol: u.protocol,
        host: u.hostname,
        path: u.path,
        port: u.port,
        method: method,
        headers: {},
    }
}

const makeRequest = opts => opts.protocol === "http:" ?
    http.request(opts) :
    https.request(opts)

const makePromise =
    (request, progressCallback) => new Promise((resolve, reject) => {
    let uploadSize = request._headers["content-length"]
    let uploadProgress = 0
    let progressInterval

    request.on("response", response => {
        let body = ""
        response.on("data", data => {
            body += data
        })
        response.on("end", () => {
            if (typeof progressInterval !== "undefined") {
                clearInterval(progressInterval)
            }
            resolve(body)
        })
    })

    request.on("error", error => reject(error))

    if (!progressCallback) return
    request.on("socket", socket => {
        let progressUpdater = () => {
            if (uploadProgress === socket.bytesWritten) return
            uploadProgress = socket.bytesWritten
            if (uploadProgress > uploadSize) uploadSize = uploadProgress
            progressCallback(uploadProgress, uploadSize)
        }
        progressInterval = setInterval(progressUpdater, 500)
    })
})


/**
 * @function post
 * Posts a JSONRPC object as JSON to an endpoint. Returns a Promise.
 * @param {string} endpoint - HTTP/HTTPS endpoint
 * @param {object} json - Object to be sent as JSON
 * @param {object} extraHeaders - Additional headers to be sent with the request
 */
const post = function(endpoint, jsonrpc, extraHeaders) {
    let body = JSON.stringify(jsonrpc)
    let opts = getOpts("POST", endpoint)
    opts.headers["Content-Type"] = "application/json"
    opts.headers["Content-Length"] = Buffer.byteLength(body)
    Object.getOwnPropertyNames(extraHeaders || {}).forEach(header => {
        opts.headers[header] = extraHeaders[header]
    })
    let request = makeRequest(opts)
    let promise = makePromise(request)
    request.write(body)
    request.end()
    return promise
}


/**
 * @function upload
 * Uploads files with a JSONRPC object as JSON to an endpoint as
 * multipart/form-data.
 * Returns a Promise.
 * @param {string} endpoint - HTTP/HTTPS endpoint
 * @param {object} jsonrpc - Object to be sent as JSON
 * @param {object} extraHeaders - Additional headers to be sent with the request
 */
const upload = function(endpoint, files, jsonrpc, extraHeaders, progressCallback) {
    let form = new FormData()

    if (jsonrpc) {
        let body = JSON.stringify(jsonrpc)
        form.append("jsonrpc", body)
    }

    files.forEach((file, i) => {
        if (typeof file === "string") {
            let stream = fs.createReadStream(file)
            let size = fs.statSync(file).size
            form.append(`file_${i}`, stream, {knownLength: size})
            return
        }
        let stream = fs.createReadStream(file.path)
        let filename = file.name || path.basename(file.path)
        let size = fs.statSync(file.path).size
        form.append(file.key, stream, {
            knownLength: size,
            filename: filename
        })
    })

    let opts = getOpts("POST", endpoint)
    let formHeaders = form.getHeaders({
        "Content-Length": form.getLengthSync()
    })
    Object.getOwnPropertyNames(formHeaders).forEach(key => {
        opts.headers[key] = formHeaders[key]
    })
    Object.getOwnPropertyNames(extraHeaders || {}).forEach(header => {
        opts.headers[header] = extraHeaders[header]
    })

    let request = makeRequest(opts)
    let promise = makePromise(request, progressCallback)
    form.pipe(request)
    return promise
}

module.exports = {
    post: post,
    upload: upload
}
