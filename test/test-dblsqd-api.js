"use strict"
/*jshint esversion:6*/
/*jslint node: true */

const {test} = require("ava")

const fs = require("fs")
const os = require("os")
const path = require("path")
const url = require("url")
const AWS = require('aws-sdk')
const SFTPS3Server = require('node-sftp-s3').SFTPS3Server

const api = require("../lib/dblsqd-api.js")
const jsonrpc = require("../lib/dblsqd-api/jsonrpc.js")

//Helper functions
const rand = () => Math.floor(Math.random() * 10000000)
const loadFile = filename => fs.readFileSync(path.join(__dirname, filename))
const uuidRegex = /^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i

//Globals
const config = JSON.parse(loadFile("test-config.json"))
var shared = {}
var tempFiles = []

test.before(async t => {
    let reset = jsonrpc.post(config.endpoint, "resetTestDB", [])

    let bucketName = `dblsqd-api-test-${rand()}`
    let s3Config = config.s3
    s3Config.params = {Bucket: bucketName}
    let s3 = new AWS.S3(s3Config)
    let bucket = new Promise((resolve, reject) => {
        s3.createBucket({Bucket: bucketName}, () => {
            resolve()
        })
    })
    await bucket

    let sftpServer = new SFTPS3Server(s3);
    let hostKey = loadFile("test-server-key")
    let userKey = loadFile("test-user-key.pub")
    sftpServer.addHostKey(hostKey);
    sftpServer.addPublicKey(userKey, config.sftp.user)
    sftpServer.listen(config.sftp.port, config.sftp.host)

    await reset
})

test.beforeEach(t => {
    api.init(shared.apiKey, config.endpoint)
})

test.afterEach(t => {
    tempFiles.forEach(path => fs.unlinkSync(path))
    tempFiles = []
})

/*
Authentication
*/
test.serial("register", async t => {
    const email = config.fixtures.email
    const password = config.fixtures.password
    const userData = {
        first_name: config.fixtures.first_name,
        last_name: config.fixtures.last_name,
        company: config.fixtures.company,
        test_force_verified: true
    }
    const response = await api.register(email, password, userData, config.endpoint)
    t.is(response, true)
})

test.serial("getApiKey", async t => {
    const email = config.fixtures.email
    const password = config.fixtures.password
    const apiKey = await api.getApiKey(email, password, config.endpoint)
    t.regex(apiKey, uuidRegex)
    shared.apiKey = apiKey
})

test.serial("revokeApiKey", async t => {
    const email = config.fixtures.email
    const password = config.fixtures.password
    const apiKey = await api.getApiKey(email, password, config.endpoint)
    api.init(apiKey, config.endpoint)
    const response = await api.revokeApiKey()
    
    t.is(response, true)
})


/*
Creating Records
*/
test.serial("createApp", async t => {
    const name = "my-app"
    const app = await api.createApp(name)

    t.regex(app.uuid, uuidRegex)
    t.is(app.name, name)
    shared.app = app
})

test.serial("createChannel", async t => {
    const name = "my-channel"
    const channel = await api.createChannel(shared.app.uuid, name)

    t.regex(channel.uuid, uuidRegex)
    t.is(channel.name, name)
    shared.channel = channel
})

test.serial("createRelease", async t => {
    const version = "1.0.0"
    const changelog = "This is the changelog"
    const release = await api.createRelease(shared.channel.uuid, version, changelog)

    t.regex(release.uuid, uuidRegex)
    t.is(release.version, version)
    t.is(release.changelog, changelog)
    shared.release = release
})

test.serial("createStoreSFTP", async t => {

    const userKey = fs.readFileSync(path.join(__dirname, 'test-user-key')).toString()
    const name = "sftp-store"
    const store = await api.createStoreSFTP("sftp-store", {
        host: "localhost",
        port: 9001,
        user_key: userKey,
        user: "foo",
        verify_host_key: false
    })

    t.regex(store.uuid, uuidRegex)
    t.is(store.name, name)
    shared.storeSFTP = store
})

test.serial("createStoreS3", async t => {
    const name = "s3-store"
    const store = await api.createStoreS3("s3-store", {
        host: "localhost",
        port: 9000,
        access_key_id: "accessKey1",
        secret_access_key: "verySecretKey1",
        bucket: "test-bucket",
        https: false
    })

    t.regex(store.uuid, uuidRegex)
    t.is(store.name, name)
    shared.storeS3 = store
})


test.serial("createArtifact S3", async t => {
    const filePath = path.join(os.tmpdir(), `dblsqd-api-test-${rand()}`)
    tempFiles.push(filePath)
    fs.writeFileSync(filePath, config.fixtures.artifactContent)

    var progress, total
    const artifact = await api.createArtifact(shared.release.uuid, shared.storeSFTP.uuid, filePath, {
        os: "linux",
        arch: "x86_64",
        type: "test"
    }, (p, t) => {
        progress = p
        total = t
    })

    t.truthy(total)
    t.is(progress, total)
    t.regex(artifact.uuid, uuidRegex)
    t.is(artifact.os, "linux")
    shared.artifact = artifact
})

/*
Listing Records
*/

test.serial("listApps", async t => {
    const apps = await api.listApps()
    let appFound = apps.find(app => app.uuid === shared.app.uuid)
    t.truthy(appFound)
})

test.serial("listChannels", async t => {
    const channels = await api.listChannels(shared.app.uuid)
    let channelFound = channels.find(channel => channel.uuid === shared.channel.uuid)
    t.truthy(channelFound)
})

test.serial("listReleases", async t => {
    const releases = await api.listReleases(shared.channel.uuid)
    let releaseFound = releases.find(release => release.uuid === shared.release.uuid)
    t.truthy(releaseFound)
})

test.serial("listArtifacts", async t => {
    const artifacts = await api.listArtifacts()
    let artifactFound = artifacts.find(artifact => artifact.uuid === shared.artifact.uuid)
    t.truthy(artifactFound)
})

test.serial("listStores", async t => {
    const stores = await api.listStores()
    let storeFoundS3 = stores.find(store => store.uuid === shared.storeS3.uuid)
    let storeFoundSFTP = stores.find(store => store.uuid === shared.storeSFTP.uuid)
    t.truthy(storeFoundS3)
    t.truthy(storeFoundSFTP)
})

/*
Finding Records
*/
test.serial("findApp", async t => {
    const app = await api.findApp(shared.app.name)
    t.is(app.uuid, shared.app.uuid)
    
    const error = await t.throws(api.findApp("not-found-app"))
    t.is(error, "App not found: \"not-found-app\"")
})

test.serial("findChannel", async t => {
    const channel = await api.findChannel(shared.app.name, shared.channel.name)
    t.is(channel.uuid, shared.channel.uuid)
    
    const error = await t.throws(api.findChannel(shared.app.name, "not-found-channel"))
    t.is(error, "Channel not found: \"not-found-channel\" in \"" + shared.app.name + "\"")
})

test.serial("findRelease", async t => {
    const release = await api.findRelease(shared.app.name, shared.channel.name, shared.release.version)
    t.is(release.uuid, shared.release.uuid)
    
    const error = await t.throws(api.findRelease(shared.app.name, shared.channel.name, "0.0.0"))
    t.is(error, "Release not found: \"0.0.0\" in \"" + shared.channel.name + "\" in \"" + shared.app.name + "\"")
})

test.serial("findStore", async t => {
    const store = await api.findStore(shared.storeS3.name)
    t.is(store.uuid, shared.storeS3.uuid)

    const error = await t.throws(api.findStore("not-found-store"))
    t.is(error, "Store not found: \"not-found-store\"")
})


/*
Deleting Records
*/
test.serial("deleteApp", async t => {
    const app = await api.createApp("delete-app-app")
    const deleted = await api.deleteApp(app.uuid)
    t.is(deleted.uuid, app.uuid)

    const apps = await api.listApps()
    let appFound = apps.find(a => a.uuid === app.uuid)
    t.is(appFound, undefined)
})

test.serial("deleteChannel", async t => {
    const app = await api.createApp("delete-channel-app")
    const channel = await api.createChannel(app.uuid, "my-channel")

    const deleted = await api.deleteChannel(channel.uuid)
    t.is(deleted.uuid, channel.uuid)

    const channels = await api.listChannels(app.uuid)
    let channelFound = channels.find(c => c.uuid === channel.uuid)
    t.is(channelFound, undefined)
})

test.serial("deleteRelease", async t => {
    const app = await api.createApp("delete-release-app")
    const channel = await api.createChannel(app.uuid, "my-channel")
    const release = await api.createRelease(channel.uuid, "1.0.0", "Changelog")

    const deleted = await api.deleteRelease(release.uuid)
    t.is(deleted.uuid, release.uuid)

    const releases = await api.listReleases(channel.uuid)
    let releaseFound = releases.find(r => r.uuid === release.uuid)
    t.is(releaseFound, undefined)
})

test.serial("deleteStore", async t => {
    const deleted = await api.deleteStore(shared.storeSFTP.uuid)
    t.is(deleted.uuid, shared.storeSFTP.uuid)

    const stores = await api.listStores()
    let storeFound = stores.find(store => store.uuid === shared.storeSFTP.uuid)
    t.is(storeFound, undefined)
})

test.serial("deleteArtifact", async t => {
    const deleted = await api.deleteArtifact(shared.artifact.uuid)
    t.is(deleted.uuid, shared.artifact.uuid)

    const artifacts = await api.listArtifacts(shared.release.uuid)
    let artifactFound = artifacts.find(artifact => artifact.uuid === shared.artifact.uuid)
    t.is(artifactFound, undefined)
})

/*
Miscellaneous
*/
test.serial("getAppBaseUrl", async t => {
    const baseUrl = await api.getAppBaseUrl(shared.app.uuid)
    t.truthy(url.parse(baseUrl))
})